<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //allow mass assignments
    protected $fillable = [
        'name', 'email', 'number', 'address', 'salary', 'eid', 'date', 'photo'
    ];
}

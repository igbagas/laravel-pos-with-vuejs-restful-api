class Noty {

    success(){
        new Noty({
            type: 'success',
            layout: 'topRight',
            text: 'Successfully Done',
            timeout: 1000
        }).show();
    }

    alert(){
        new Noty({
            type: 'alert',
            layout: 'center',
            text: 'Are You Sure',
            timeout: 1000
        }).show();
    }

    error(){
        new Noty({
            type: 'error',
            layout: 'center',
            text: 'Something Went Wrong',
            timeout: 1000
        }).show();
    }

}

export default Noty =  new Noty()